# -*- coding: utf-8 -*-
import sys
import os
import json
import time

import requests
from PyQt5.QtCore import QDate
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QComboBox
from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QGridLayout
from PyQt5.QtWidgets import QHBoxLayout
from PyQt5.QtWidgets import QPushButton
from PyQt5.QtWidgets import QTableWidget
from PyQt5.QtWidgets import QTableWidgetItem
from PyQt5.QtWidgets import QVBoxLayout
from PyQt5.QtWidgets import QLineEdit
from PyQt5.QtWidgets import QLabel
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QAbstractItemView
from PyQt5.QtWidgets import QMessageBox

DATE_FORMAT = "yyyy年MM月dd日"


class Prescription(QWidget):
    def __init__(self):
        super().__init__()
        self.drug_list = []
        self.office_list = []
        self.pay_list = []
        self.use_list = []
        self.dos_list = []
        self.drugs = []

        # 输入组件
        self.url_lab = QLabel('URL：')
        self.url = QLineEdit()
        self.getButton = QPushButton("新建处方")
        self.num_lab = QLabel('登记号：')
        self.num = QLineEdit()
        self.num.setEnabled(False)
        self.pre_lab = QLabel('处方号：')
        self.pre = QLineEdit()
        self.pre.setEnabled(False)
        self.tp_lab = QLabel('科别：')
        self.tp = QComboBox()
        self.name_lab = QLabel('姓名：')
        self.name = QLineEdit()
        self.gender_lab = QLabel('性别：')
        self.gender = QComboBox()
        self.gender.addItem("")
        self.gender.addItem("")
        self.gender.setItemText(0, '男')
        self.gender.setItemText(1, '女')
        self.age_lab = QLabel('年龄：')
        self.age = QLineEdit()
        self.prc_lab = QLabel('费别：')
        self.prc = QComboBox()
        self.res_lab = QLabel('临床诊断：')
        self.res = QLineEdit()
        self.drug_lab = QLabel('药品目录：')
        self.search = QLineEdit()
        self.seaButton = QPushButton("搜索")
        self.detail_lab = QLabel('处方明细：')
        # table
        self.detail_table = QTableWidget()
        self.detail_table.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.detail_table.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.drug_table = QTableWidget()
        # 按键
        self.clearButton = QPushButton("清空数据")
        self.painterButton = QPushButton("预览")
        self.quitButton = QPushButton("退出")

        # 实例化网格布局
        self.grid = QGridLayout()
        self.grid.setSpacing(10)
        # 布局添加输入框
        self.grid.addWidget(self.url_lab, 1, 0)
        self.grid.addWidget(self.url, 1, 1)
        self.grid.addWidget(self.getButton, 1, 2)
        self.grid.addWidget(self.num_lab, 2, 0)
        self.grid.addWidget(self.num, 2, 1)
        self.grid.addWidget(self.pre_lab, 2, 2)
        self.grid.addWidget(self.pre, 2, 3)
        self.grid.addWidget(self.tp_lab, 2, 4)
        self.grid.addWidget(self.tp, 2, 5)
        self.grid.addWidget(self.name_lab, 3, 0)
        self.grid.addWidget(self.name, 3, 1)
        self.grid.addWidget(self.gender_lab, 3, 2)
        self.grid.addWidget(self.gender, 3, 3)
        self.grid.addWidget(self.age_lab, 3, 4)
        self.grid.addWidget(self.age, 3, 5)
        self.grid.addWidget(self.prc_lab, 4, 0)
        self.grid.addWidget(self.prc, 4, 1)
        self.grid.addWidget(self.res_lab, 4, 2)
        self.grid.addWidget(self.res, 4, 3)
        self.grid.addWidget(self.drug_lab, 5, 0)
        self.grid.addWidget(self.search, 5, 1)
        self.grid.addWidget(self.seaButton, 5, 2)
        self.grid.addWidget(self.detail_lab, 5, 3)
        # 布局添加数据表
        self.grid.addWidget(self.detail_table, 6, 0, 6, 3)
        self.grid.addWidget(self.drug_table, 6, 3, 6, 3)

        # 实例化按键布局
        self.buttonLayout = QHBoxLayout()
        # 按键布局添加按键
        self.buttonLayout.addWidget(self.clearButton)
        self.buttonLayout.addWidget(self.painterButton)
        self.buttonLayout.addWidget(self.quitButton)

        # 实例化布局
        self.layout = QVBoxLayout()
        # 布局添加登记号
        self.layout.addLayout(self.grid)
        # 布局添加按键布局
        self.layout.addLayout(self.buttonLayout)
        self.setLayout(self.layout)

        self.setGeometry(300, 300, 1280, 640)
        self.setWindowTitle('国药控股处方打印程序')

        # 按键绑定事件
        self.seaButton.clicked.connect(self.search_drug)
        self.getButton.clicked.connect(self.get_data)
        self.clearButton.clicked.connect(self.clear_data)
        self.painterButton.clicked.connect(self.print_html)
        self.quitButton.clicked.connect(self.close)

    def search_drug(self):
        key_word = self.search.text()
        url = self.url.text()
        if url is not '':
            sea_url = 'http://' + url + '/drug/?search=' + key_word
            self.drug_list = json.loads(requests.get(sea_url).text)
            self.detail_table.clear()
            # 遍历循环每列数据
            for row, drug in enumerate(self.drug_list):
                addDrug = QPushButton('添加')
                addDrug.setMaximumWidth(50)
                addDrug.index = drug
                addDrug.clicked.connect(self.add_drug)
                self.detail_table.setItem(row, 0, QTableWidgetItem(drug['name']))
                self.detail_table.setItem(row, 1, QTableWidgetItem(drug['unit']))
                self.detail_table.setItem(row, 2, QTableWidgetItem(drug['spec']))
                self.detail_table.setCellWidget(row, 3, addDrug)
        else:
            reply = QMessageBox.warning(self, '警告', 'URL不正确', QMessageBox.Yes, QMessageBox.No)

    def get_data(self):
        # 清空数据
        self.clear_data()
        # url处理
        url = self.url.text()
        if url is not '':
            try:
                response = requests.get('http://' + url + '/')
                if response.status_code == 200:
                    drug_url = 'http://' + url + '/drug/'
                    office_url = 'http://' + url + '/offices/'
                    pay_url = 'http://' + url + '/pay/'
                    use_url = 'http://' + url + '/use/'
                    dos_url = 'http://' + url + '/dos/'
                    self.drug_list = json.loads(requests.get(drug_url).text)
                    self.office_list = json.loads(requests.get(office_url).text)
                    self.pay_list = json.loads(requests.get(pay_url).text)
                    self.use_list = json.loads(requests.get(use_url).text)
                    self.dos_list = json.loads(requests.get(dos_url).text)

                    # 新建处方号和登记号
                    ct = time.time()
                    ms = int((ct - int(ct)) * 10)
                    now = '18' + time.strftime('%m%d%H%M%S', time.localtime(ct)) + str(ms)

                    self.num.setText('D' + now)
                    self.pre.setText('C' + now)

                    # 科别
                    for i, item in enumerate(self.office_list):
                        self.tp.addItem('')
                        self.tp.setItemText(i, item['name'])

                    # 费别
                    for i, item in enumerate(self.pay_list):
                        self.prc.addItem('')
                        self.prc.setItemText(i, item['name'])

                    # 药品列表
                    headers = ["药品名称", "单位", "规格", "操作"]
                    # 列数
                    self.detail_table.setColumnCount(len(headers))
                    # 设置table表头
                    self.detail_table.setHorizontalHeaderLabels(headers)
                    # 列宽
                    self.detail_table.setColumnWidth(0, 200)
                    self.detail_table.setColumnWidth(1, 100)
                    self.detail_table.setColumnWidth(2, 100)
                    self.detail_table.setColumnWidth(3, 50)
                    # 行数
                    self.detail_table.setRowCount(len(self.drug_list))
                    # 遍历循环每列数据
                    for row, drug in enumerate(self.drug_list):
                        addDrug = QPushButton('添加')
                        addDrug.setMaximumWidth(50)
                        addDrug.index = drug
                        addDrug.clicked.connect(self.add_drug)
                        self.detail_table.setItem(row, 0, QTableWidgetItem(drug['name']))
                        self.detail_table.setItem(row, 1, QTableWidgetItem(drug['unit']))
                        self.detail_table.setItem(row, 2, QTableWidgetItem(drug['spec']))
                        self.detail_table.setCellWidget(row, 3, addDrug)
                    # 自动调整大小
                    # self.detail_table.resizeColumnsToContents()

                    # 明细列表
                    headers = ["药品名称", "单位", "规格", '数量', '单次剂量', '用法', '操作']
                    # 列数
                    self.drug_table.setColumnCount(len(headers))
                    # 设置table表头
                    self.drug_table.setHorizontalHeaderLabels(headers)
                    # 行宽
                    self.drug_table.setColumnWidth(0, 200)
                    self.drug_table.setColumnWidth(1, 80)
                    self.drug_table.setColumnWidth(2, 80)
                    self.drug_table.setColumnWidth(3, 80)
                    self.drug_table.setColumnWidth(4, 110)
                    self.drug_table.setColumnWidth(5, 110)
                    self.drug_table.setColumnWidth(6, 50)
                    # 行数
                    self.drug_table.setRowCount(0)
                    # 自动调整大小
                    # self.drug_table.resizeColumnsToContents()
                else:
                    reply = QMessageBox.warning(self, '警告', 'URL不正确', QMessageBox.Yes, QMessageBox.No)
            except:
                reply = QMessageBox.warning(self, '警告', 'URL不正确', QMessageBox.Yes, QMessageBox.No)
        else:
            reply = QMessageBox.warning(self, '警告', 'URL不正确', QMessageBox.Yes, QMessageBox.No)

    def del_drug(self):
        self.drug_table.removeRow(self.drug_table.currentRow())

    def add_drug(self):
        if self.drug_table.rowCount() >= 5:
            reply = QMessageBox.warning(self, '警告', '处方明细不能超过5条，如需超过请新开另外一张处方', QMessageBox.Yes, QMessageBox.No)
        else:
            use = QComboBox()
            dos = QComboBox()

            # 用法
            for i, item in enumerate(self.use_list):
                use.addItem('')
                use_str = item['use'] + '/' + item['use_unit']
                use.setItemText(i, use_str)

            # 单次剂量
            for i, item in enumerate(self.dos_list):
                dos.addItem('')
                dos.setItemText(i, item['dos'])

            index = self.drug_table.rowCount()
            delDrug = QPushButton('删除')
            delDrug.setMaximumWidth(50)
            delDrug.index = index
            delDrug.clicked.connect(self.del_drug)
            self.drug_table.setRowCount(index + 1)
            self.drug_table.setItem(index, 0, QTableWidgetItem(self.sender().index['name']))
            self.drug_table.setItem(index, 1, QTableWidgetItem(self.sender().index['unit']))
            self.drug_table.setItem(index, 2, QTableWidgetItem(self.sender().index['spec']))
            self.drug_table.setItem(index, 3, QTableWidgetItem(''))
            self.drug_table.setCellWidget(index, 4, dos)
            self.drug_table.setCellWidget(index, 5, use)
            self.drug_table.setCellWidget(index, 6, delDrug)
            # 禁止列编辑
            self.drug_table.item(index, 0).setFlags(Qt.ItemIsEnabled)
            self.drug_table.item(index, 1).setFlags(Qt.ItemIsEnabled)
            self.drug_table.item(index, 2).setFlags(Qt.ItemIsEnabled)

    def clear_data(self):
        self.num.setText('')
        self.pre.setText('')
        self.tp.clear()
        self.name.setText('')
        self.age.setText('')
        self.prc.clear()
        self.res.setText('')
        self.detail_table.clear()
        self.drug_table.clear()
        self.drug_list = []
        self.office_list = []
        self.pay_list = []
        self.drugs = []

    def print_html(self):
        for row in range(self.drug_table.rowCount()):
            self.drugs.append((self.drug_table.item(row, 0).text(),
                               self.drug_table.item(row, 1).text(),
                               self.drug_table.item(row, 2).text(),
                               self.drug_table.item(row, 3).text(),
                               self.drug_table.cellWidget(row, 4).currentText(),
                               self.drug_table.cellWidget(row, 5).currentText()))

        htmltext = ("""
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>成都医学院第一附属医院处方笺</title>
</head>
<body style="margin: auto; width: 210mm;height: 148mm;padding-left: 10mm;">
<!--startprint1-->
<!--打印内容开始-->
<h1 style="background: url('code.png') no-repeat;background-size: 41mm 12mm;background-position: left;text-align: center;font-size: 7.8mm;margin: auto;line-height: 12mm;">
    成都医学院第一附属医院处方笺</h1>

<div style="margin-left: 16mm;margin-top: 1mm;font-size: 4mm;">
    <div style="height: 7mm">
        <div style="float:left;width: 15mm">登记号</div>
        <div style="margin-left: 2mm;float:left;font-weight: bold;width: 35mm">{0}</div>
        <div style="margin-left: 2mm;float:left;width: 15mm">处方号</div>
        <div style="margin-left: 2mm;float:left;font-weight: bold;width: 35mm">{1}</div>
        <div style="margin-left: 2mm;float:left;width: 10mm">科别</div>
        <div style="margin-left: 2mm;float:left;font-weight: bold;width: 25mm">{2}</div>
        """).format(self.num.text(), self.pre.text(), self.tp.currentText())
        date = QDate.currentDate().toString(DATE_FORMAT)
        htmltext += ("""
        <span style="float: right;position: relative;padding-right: 12mm">{0}<span
                style="position: absolute;top: -5mm;left: 17mm;">[普通]</span></span>
    </div>
    <div style="height: 7mm">
        <div style="float:left;width: 15mm">姓名</div>
        <div style="margin-left: 3mm;float:left;font-weight: bold;width: 17mm">{1}</div>
        <div style="margin-left: 10mm;float:left;width: 15mm">性别</div>
        <div style="float:left;font-weight: bold;width: 15mm">{2}</div>
        <div style="margin-left: 3mm;float:left;width: 15mm">年龄</div>
        <div style="margin-left: 3mm;float:left;font-weight: bold;width: 10mm">{3}岁</div>
        <div style="float: right">
            <div style="float:left;width: 10mm">费别</div>
            <div style="padding-right: 13mm;float:left;font-weight: bold;    margin-left: 3mm;">{4}</div>
        </div>
    </div>
    <div style="margin-bottom: 4mm;">
        <span>临床诊断：1. {5}</span>
    </div>
</div>

<div style="margin-left: 15mm;height: 95mm;border-top: black 1px solid;border-bottom: black 1px solid;">
    <span style="float: left;font-size: 5mm;font-weight: bold;line-height: 7mm;">R:</span>
    <div style="width: 120mm;margin-left:11mm;margin-top: 2mm;font-size: 4mm">
        """).format(date, self.name.text(), self.gender.currentText(), self.age.text(), self.prc.currentText(),
                    self.res.text())

        for row, drug in enumerate(self.drugs):
            htmltext += ("""
        <div style="height: 16mm;">
            <div style="height: 8mm;">
                <div style="float: left;">{0}</div>
                <div style="float: left;margin-left: 3mm;">{1}</div>
                <div style="float: left;margin-left: 12.5mm;">{3}/{2}×{4}{2}</div>
            </div>
            <div style="height: 8mm;margin-left: 12.5mm">
                <div style="float: left;">用法：</div>
                <div style="float: left;">{5}</div>
                <div style="float: left;margin-left: 12.5mm">{6}</div>
            </div>
        </div>
            """).format(row + 1, drug[0], drug[1], drug[2], drug[3], drug[4], drug[5])

        htmltext += ("""
        <div style="text-align: center;">------------------（处方完毕）----------------</div>
    </div>
</div>
<div style="margin-left: 16mm;margin-top: 1mm">
    <div style="font-size: 4mm">
        <span>医师</span>
        <span style="margin-left: 65mm">签章/签名</span>
        <span style="margin-left: 50mm">金额</span>
    </div>
    <div style="font-size: 4mm">
        <span>药师（审核、核对、发药）签章/签名</span>
        <span style="margin-left: 35mm">药师/士（调配）签章/签名</span>
    </div>
    <div style="font-size: 4mm">药师提示：1、请遵医嘱服药；2、请在窗口清点药品；3、处方当日有效。</div>
    <div style="font-size: 3mm;font-weight: bold">特别提示：按卫生部、国家中医药管理局卫医政发【2011】11号文件规定：为保证患者用药安全，药品一经发出，不得退换。</div>
</div>
</div>
<!--打印内容结束-->
<!--endprint1-->
<div style="text-align: center;margin-top: 20px">
    <input type=button name='button_export' title='打印' onclick=preview(1) value="打印">
</div>
<script language="javascript">
    function preview(oper) {
        if (oper < 10) {
            bdhtml = window.document.body.innerHTML;//获取当前页的html代码
            sprnstr = "<!--startprint" + oper + "-->";//设置打印开始区域
            eprnstr = "<!--endprint" + oper + "-->";//设置打印结束区域
            prnhtml = bdhtml.substring(bdhtml.indexOf(sprnstr) + 18); //从开始代码向后取html
            prnhtml = prnhtml.substring(0, prnhtml.indexOf(eprnstr));//从结束代码向前取html
            window.document.body.innerHTML = prnhtml;
            window.print();
            window.document.body.innerHTML = bdhtml;
        } else {
            window.print();
        }
    }
</script>
</body>
</html>
        """)

        with open('print.html', 'w', encoding='utf-8') as f:
            f.truncate()
            f.write(htmltext)
            cmd = 'explorer print.html'
            os.system(cmd)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    prescription = Prescription()
    prescription.show()
    app.exec_()
